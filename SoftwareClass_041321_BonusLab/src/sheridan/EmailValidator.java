package sheridan;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailValidator {
	
	public static boolean hasValidAccount(String account) {
		
		if(Character.isDigit(account.charAt(0))) {
			return false;
		}
		else if(account == account.toUpperCase()) {
			return false;
		}

		return account.indexOf(" ") < 0 && account.length() >= 3 && account.matches("^[a-z0-9]*$");
	}
	
	public static boolean hasValidDomain(String domain) {
		
		if(domain == domain.toUpperCase()) {
			return false;
		}

		return domain.indexOf(" ") < 0 && domain.length() >= 3 && domain.matches("^[a-z0-9]*$");
	}
	
	public static boolean hasValidExtension(String extension) {
		
		if(extension == extension.toUpperCase()) {
			return false;
		}

		return extension.indexOf(" ") < 0 && extension.length() >= 2 && extension.matches("^[a-z]*$");
	}
	
	public static boolean hasValidFormat(String email) {	
		
		
		String regex = "^[a-z0-9]*\\@[a-z0-9]*\\.[a-z0-9]*$";
		
		if(email.indexOf(" ") < 0) {
			
			Pattern p = Pattern.compile(regex);
			Matcher m = p.matcher(email);
			return m.find();
		}
		
		return false;
	}
	
	public static boolean hasValidAt(String email) {
		
		int count = 0;
		for(int i = 0; i < email.length(); i++) {
			if(email.charAt(i) ==  '@'){
				count++;
			}
		}
		
		return email.indexOf(" ") < 0 && count == 1;
	}
	
}
