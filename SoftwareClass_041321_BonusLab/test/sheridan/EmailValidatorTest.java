package sheridan;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class EmailValidatorTest {

//	@Test
//	public void testEmailHasValidFormatRegular() {
//		
//		fail("Not yet implemented");
//	}
	
	// Test for Account	
	@Test
	public void testEmailHasValidAccountRegular() {		
		assertTrue("Invalid Account", EmailValidator.hasValidAccount("abc123"));
	}
	
	@Test
	public void testEmailHasValidAccountException() {		
		assertFalse("Invalid Account", EmailValidator.hasValidAccount("!@#$%"));
	}
	
	@Test
	public void testEmailHasValidAccountBoundaryIn() {		
		assertTrue("Invalid Account", EmailValidator.hasValidAccount("z10"));
	}
	
	@Test
	public void testEmailHasValidAccountBoundaryOut() {		
		assertFalse("Invalid Account", EmailValidator.hasValidAccount("1ab"));
	}
	
	
	// Test for Domain	
	@Test
	public void testEmailHasValidDomainRegular() {		
		assertTrue("Invalid Account", EmailValidator.hasValidDomain("abc123"));
	}
	
	@Test
	public void testEmailHasValidDomainException() {		
		assertFalse("Invalid Account", EmailValidator.hasValidDomain("!@#$%"));
	}
	
	@Test
	public void testEmailHasValidDomainBoundaryIn() {		
		assertTrue("Invalid Account", EmailValidator.hasValidDomain("1za"));
	}
	
	@Test
	public void testEmailHasValidDomainBoundaryOut() {		
		assertFalse("Invalid Account", EmailValidator.hasValidDomain("b1A"));
	}
	
	
	// Test for Extension
	@Test
	public void testEmailHasValidExtensionRegular() {		
		assertTrue("Invalid Account", EmailValidator.hasValidExtension("abc"));
	}
	
	@Test
	public void testEmailHasValidExtensionException() {		
		assertFalse("Invalid Account", EmailValidator.hasValidExtension("!@#$%"));
	}
	
	@Test
	public void testEmailHasValidExtensionBoundaryIn() {		
		assertTrue("Invalid Account", EmailValidator.hasValidExtension("ab"));
	}
	
	@Test
	public void testEmailHasValidExtensionBoundaryOut() {		
		assertFalse("Invalid Account", EmailValidator.hasValidExtension("a1"));
	}
	
	
	// Test for Format
	@Test
	public void testEmailHasValidFormatRegular() {
		String account = "abc123";
		String domain = "gmail";
		String extension = "com";				
		assertTrue("Invalid Account", EmailValidator.hasValidFormat(account + "@" + domain + "." + extension));
	}
	
	
	// Test for @At
	@Test
	public void testEmailHasValidAtRegular() {		
		assertTrue("Invalid Account", EmailValidator.hasValidAt("abc123@gmail.com"));
	}
	@Test
	public void testEmailHasValidAtExceptions() {		
		assertFalse("Invalid Account", EmailValidator.hasValidAt(" "));
	}
	@Test
	public void testEmailHasValidAtBoundaryOut() {		
		assertFalse("Invalid Account", EmailValidator.hasValidAt("abc123@@gmail.com"));
	}
	


}
